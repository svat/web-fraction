# web-fraction

Webpage for rational approximations

More or less "done" (aka abandoned), but there are some ways in which it can be improved:

- Could use Babel or something, to make it work on browsers that do not support BigInt (Safari, iOS). Should be relatively straightforward, but low priority for me personally.

- The UI could be better, e.g. make it more obvious that values can be modified, fit to screen better, etc.

- The wall of text at the top should ideally be replaced by something that is itself interactive / demonstrates the idea.

- Wrote some code for reading float values... is there any use for it?

But probably the most major issue is the following:

- Right now, the best rational approximations shown are to the exact number entered. Some of these will not be best rational approximations to the actually desired number. For example, if you enter 3.14159265 (and choose a large limit for the denominator), you get the following sequence of best rational approximations of the second kind:

```
Approximation        Decimal value                       Error
3 / 1                 3.                                 –‍0.14159265
22 / 7                3.142857142857142857142857142857…   0.001264492857142857142857142857…
333 / 106             3.141509433962264150943396226415…  –‍0.000083216037735849056603773584…
355 / 113             3.141592920353982300884955752212…   0.000000270353982300884955752212…
102573 / 32650        3.141592649310872894333843797856…  –‍0.000000000689127105666156202143…
102928 / 32763        3.141592650245703995360620211824…   0.000000000245703995360620211824…
308429 / 98176        3.141592649934810951760104302477…  –‍0.000000000065189048239895697522…
411357 / 130939       3.141592650012601287622480697118…   0.000000000012601287622480697118…
1542500 / 490993      3.141592649997046801074557071078…  –‍0.000000000002953198925442928921…
1953857 / 621932      3.141592650000321578564859180746…   0.000000000000321578564859180746…
15219499 / 4844517    3.141592649999989679053660044953…  –‍0.000000000000010320946339955046…
62831853 / 20000000   3.14159265                          (zero)
```

But of these, each fraction after 355/113 is actually not a best rational approximation to π itself (though it is one for 3.14159265). So ideally we'd like to list only the best rational approximations that are stable under modifications of the last digit of the input, say. For example, the next *real* best approximation (of the second kind) to π, namely 103993 / 33102, is one for fractions in roughly the range from 3.1415926531 to 3.1415926539.

Test cases:

- When input is 3.1415926535 (or more digits), the approximation 103993 / 33102 *should* be listed.

- When input is 3.141592653 (or fewer digits), the last approximation listed should be 355 / 113 (even for the first kind, it turns out).

From trying a few examples, it appears that if you type n digits of a fraction, then the convergents are correct up to about n/2 digits' worth. This may be an interesting theorem to prove (if it is true).

Some day I may fix that (or someone else may kindly take this over and improve it). Until then, the "bug" (a misleading fraction displayed) will remain.

Edit [2020-01-08]: Actually the simplest fix may be to show, given a fraction `x/10^n`, only show approximations that are also approximations for `(x-1/2)/10^n` and for `(x+1)/10^n`. (Asymmetric because last digit may be either rounded up, or truncated down.) Take the three lists of approximations, and find their intersection. This will avoid spurious convergents from being shown. We could show something like "no more convergents can be determined with these many digits; add more zeroes if the number is exact".
