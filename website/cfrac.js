// Continued fractions and best rational approximations

// Represent p/q as a continued fraction [a0; a1, a2, ..., an].
export function cfrac(p, q) {
  p = BigInt(p);
  q = BigInt(q);
  const [op, oq] = [p, q];
  if (q < 0) {
    p = -p;
    q = -q;
  }
  const a0 = p / q;
  [p, q] = [p - q * a0, q];
  let as = [a0];
  while (p != 0) {
    as.push(q / p);
    [p, q] = [q % p, p];
  }
  console.log(`${op}/${oq} = [${a0};${as.slice(1)}]`);
  return as;
}

// The convergents of p/q.
export function* convergents(p, q) {
  let xs = cfrac(p, q);
  let [a, b, c, d] = [0n, 1n, 1n, 0n];
  for (const x of xs) {
    [a, b, c, d] = [c, d, a + x * c, b + x * d];
    yield [c, d, true];
  }
}

// The best rational approximations of p/q, i.e. all fractions a/b such that
// a/b is closer to p/q than any fraction with a smaller denominator.
export function* bestRationalApproximations(p, q) {
  p = BigInt(p);
  q = BigInt(q);
  // Whether is a/b is closer than c/d to p/q, i.e. 
  // whether |p/q - a/b| < |p/q - c/d| i.e. d|pb - aq| < b|pd - cq|
  function closer(a, b, c, d) {
    function myAbs(x) { return x < 0 ? -x : x; }
    return myAbs(d * (p * b - a * q)) < myAbs(b * (p * d - c * q));
  }
  let xs = cfrac(p, q);
  const a0 = xs.shift();
  // We want to avoid the case where both 0/1 and 1/1 are printed.
  if (a0 == 0 && xs.length > 0 && xs[0] == 1) {
    // Don't print 0/1, as next will be 1/1.
  } else {
    yield [a0, 1n, true];
  }
  let [a, b, c, d] = [/*0n, 1n,*/ 1n, 0n, a0, 1n];
  for (let x of xs) {
    let start = x / 2n;
    let [tP, tQ] = [a + start * c, b + start * d];
    if (!closer(tP, tQ, c, d)) start += 1n;
    for (let n = start; n <= x; ++n) {
      yield [a + n * c, b + n * d, n == x];
    }
    [a, b, c, d] = [c, d, a + x * c, b + x * d];
  }
}
