// Express num/den as a string x.abcdef...
export function* decimalDigits(num, den) {
    num = BigInt(num);
    den = BigInt(den);
    // Hack: Use EN SPACE and EN DASH + ZWJ, so that the digits are aligned.
    let sign = ' '; // EN SPACE
    if (num < 0) {
        sign = '–‍'; // EN DASH followed by ZWJ
        num = -num;
    }
    if (num == 0) {
        yield `${sign}(zero)`;
        return;
    }
    yield `${sign}${num / den}.`;
    num = num % den;
    while (num > 0) {
        num *= 10n;
        yield `${num / den}`;
        num = num % den;
    }
}

// Non-generator version of above with fixed length l.
// If passed, |sawNonZero|'s |saw| property is set to any nonzero digit seen.
export function firstDigits(num, den, l, sawNonZero) {
    if (num == 0 && sawNonZero != null) {
        sawNonZero.saw = '(zero)';
    }
    let digits = '';
    let seen = 0;
    for (let x of decimalDigits(num, den)) {
        seen += 1;
        if (seen > l) {
            digits += "…";
            break;
        }
        digits += x;
        if (seen > 1 && sawNonZero != null && `${x}` != '0') {
            sawNonZero.saw = x;
        }
    }
    return digits;
}
