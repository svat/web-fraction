// Returns the Farey sequence of order n, i.e the ordered sequence of
// all fractions 0 <= f <= 1 with denominator at most n.
export function* farey(n) {
    n = BigInt(n);
    // a/b and c/d are the first two terms of the sequence
    let [a, b, c, d] = [0n, 1n, 1n, n];
    yield [a, b];
    while (c <= n) {
        const k = (n + b) / d;
        [a, b, c, d] = [c, d, k * c - a, k * d - b];
        yield [a, b];
    }
}