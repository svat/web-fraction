import { floatParts } from "./float.js";  // Needed only for parseFracFromFloat.

function gcd(a, b) {
    return b == 0 ? (a < 0 ? -a : a) : gcd(b, a % b);
}

function lowestTerms(p, q) {
    const g = gcd(p, q);
    return [p / g, q / g];
}

// Parse a string like "0.4" or "1/3" or even "0.25 / 7 / .35" into an exact fraction.
// Returns either |null| or a pair of |BigInt|s.
export function parseFracFromString(s) {
    const slash = s.lastIndexOf('/');
    if (slash != -1) {
        const lhs = parseFracFromString(s.slice(0, slash));
        const rhs = parseFracFromString(s.slice(slash + 1));
        if (lhs == null || rhs == null) { return null; }
        // (a/b) / (c/d) = (ad)/(bc)
        const [a, b] = lhs;
        const [c, d] = rhs;
        return lowestTerms(a * d, b * c);
    }
    s = s.trim();
    if (s.length == 0) return null;
    let seenDot = false;
    let num = 0n;
    let den = 1n;
    for (let c of s) {
        if (c == '.') {
            if (seenDot) return null;
            seenDot = true;
            continue;
        }
        if ('0' <= c && c <= '9') {
            num = num * 10n + BigInt(c - '0');
        } else {
            return null;
        }
        if (seenDot) den = den * 10n;
    }
    return lowestTerms(num, den);
}

// A floating-point x as an exact numerator/denominator
export function parseFracFromFloat(x) {
    const { sign, exponent, significand } = floatParts(x);
    let num = sign * significand;
    let den = 1n;
    if (exponent > 0) {
        for (let i = 0; i < exponent; ++i) num *= 2n;
    } else {
        for (let i = 0; i < -exponent; ++i) den *= 2n;
    }
    return lowestTerms(num, den);
}

