<!DOCTYPE html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Fraction</title>
<style>
    #fractions th,
    #fractions td,
    #approximations th,
    #approximations td {
        padding: 0px 8px;
        border: 1px dotted rgba(206, 200, 197, 0.899);
    }

    div {
        padding-bottom: 1em;
    }
</style>

<!-- <div>Here are some small fractions:</div>
<div id="fractions"></div> -->

<div>Suppose we have a (real) number x and we want to approximate it by a fraction. For example, π ≈ 3.14159265… could
    be approximated as 22/7, or as 314159/100000. In fact, for each denominator q = 1, 2, 3, …, we could approximate x
    by a fraction p/q, where the best choice for p is the integer closest to xq. Roughly speaking, we can get closer
    approximations by using larger denominators.</div>

<div>If we fix a certain “budget” of how large the denominator is allowed to be, then what is the closest approximation
    we can achieve? Usually, the answer is not simply to use the largest denominator: for example, rather than
    choosing the denominator 100 to approximate π as 314/100=3.14, it is better to choose the smaller denominator 99 and
    approximate π as 311/99≈3.14141414…. Such fractions that give the best result for a given “budget” are
    called <em>best rational approximations</em>. We can state this more precisely:</div>

<div><strong>Definition: </strong>Given a number x, its <em>best rational approximations of the first kind</em> are
    those fractions p/q such that x is closer to p/q than to any fraction with a smaller denominator.
    In other words, for any fraction p′/q′ with q′ &lt; q, we have |x-p′/q′| > |x-p/q|.</div>

<div>For π, the fraction 311/99 is a best rational approximation (of the first kind), while 314/100 is not. Apart from
    asking for the best fraction under budget, we can also ask for the best “bang for the buck”. Is it worth doubling
    our denominator budget, say, if the error doesn't even get halved? For example when we move from 22/7 to 311/99, the
    denominator increased by over 14 times, while the error only decreased by about 7 times. So relative to the
    denominator, the error in 311/99 is still somewhat large. On the other hand, 333/106 really is (slightly) better
    than 22/7 even relative to the denominator, because the denominator increased by a factor of less than 15.15, while
    the error decreased by a factor of more than 15.19. Here what we care about is not the absolute error itself, but
    the product of the error and the denominator used. To state this more precisely:</div>

<div><strong>Definition: </strong>Given a number x, its <em>best rational approximations of the second kind</em> are
    those fractions p/q such that for any fraction p′/q′ with q′ &lt; p, we have |q′x - p′| > |qx - p|.</div>

<div>These are a subset of the former: the rational approximations that are especially good even among the best rational
    approximations of the first kind. Another way of looking at them is that as we pick for each successive denominator
    q the closest integer p to xq (as mentioned in the first paragraph above), these are the cases for which the closest
    integer (p) is closer than ever before to the desired value (xq).</div>

<div>It turns out that there is a simple and fast algorithm to enumerate all such best rational approximations of any
    given number. (It also turns out that 355/113 is an amazingly good approximation of π, as it requires <em>much</em>
    larger denominators to beat it.) Here's a demonstration below (requires your browser to <a
        href="https://caniuse.com/#feat=bigint">support BigInt</a>).</div>

<div>Enter your own desired fraction (e.g. 0.25 or 1/3):&NonBreakingSpace; <input type="text" id="numberIn"
        value="3.14159265358979323846264338327950288419716939937510"></div>

<div>The fraction is <span id="numberShow"></span>.</div>
<div>Its <select name="whichBest" id="whichBest">
        <option value="best" selected>best</option>
        <option value="bestest">bestest</option>
    </select>
    rational approximations with denominator at most <input type="number" id="maxDen" value="17000">,
    and for each its decimal value and error (to <input type="number" id="errorDigits" value="5"> digits):
</div>
<table id="approximations">
    <tr>
        <th>Approximation</th>
        <th>Decimal value</th>
        <th>Error</th>
    </tr>
</table>

<div>(Caveat: These are approximations for the fraction you entered (e.g. 3.14159265), and possibly not for the actual number (e.g &pi;) you approximated by the fraction you entered. See <a href="https://gitlab.com/svat/web-fraction/blob/master/README.md">source repo</a> for details on this “bug”.)</div>

<div>How does this work? I hope to write this up in more detail later, but in short: (1) every real number can be
    represented as a <a
        href="https://en.wikipedia.org/w/index.php?title=Continued_fraction&oldid=901081149"><strong>continued
            fraction</strong></a>, (2) the <a
        href="https://en.wikipedia.org/w/index.php?title=Continued_fraction&oldid=901081149#Best_rational_approximations">best
        rational approximations</a> of the second kind are all <strong>convergents</strong> of the continued fraction,
    and (3) the best rational approximations of the first kind are, additionally, about half of the semi-convergents /
    intermediate fractions. See for example pages 22 to 28 of Khinchin's classic book on continued fractions. Or puzzle
    your way through <a href="https://gitlab.com/svat/web-fraction">the source code</a>. A related <a
        href="https://shreevatsa.wordpress.com/2011/01/10/not-all-best-rational-approximations-are-the-convergents-of-the-continued-fraction/">blog
        post</a> from a few years ago.</div>

<!-- <script src="float.js" type="module"></script> -->
<script type="module">
    import { parseFracFromString } from "./parse.js";
    import { firstDigits } from "./print.js";
    import { bestRationalApproximations, convergents } from "./cfrac.js";
    import { farey } from "./farey.js";
    // function sleepSeconds(seconds) { return new Promise(resolve => setTimeout(resolve, seconds * 1000)); }
    const slash = ' / '; // U+2009 THIN SPACE on either side

    // A HTML (<tr>) node showing the approximation n/d of num/den
    function renderFraction(n, d, num, den, errorDigits, sawNonZero, isConvergent) {
        const tr = document.createElement('tr');
        if (isConvergent) {
            tr.style.backgroundColor = 'lightgreen';
        }
        let td;

        td = document.createElement('td');
        td.appendChild(document.createTextNode(`${n}${slash}${d}`));
        tr.appendChild(td);

        td = document.createElement('td');
        td.appendChild(document.createTextNode(`${firstDigits(n, d, errorDigits)} `));
        tr.appendChild(td);

        td = document.createElement('td');
        if (num != null) {
            // n/d - num/den = (n*den - num*d)/(d*den)
            const error = firstDigits(n * den - num * d, d * den, errorDigits, sawNonZero);
            td.appendChild(document.createTextNode(error));
            tr.appendChild(td);
        }
        return tr;
    }

    function addFractions() {
        const out = document.getElementById("fractions");
        for (let [n, d] of farey(6)) {
            out.appendChild(renderFraction(n, d, null, null, 5, false));
        }
    }
    // addFractions();

    function getInputWidth(el) {
        const tmp = document.createElement('span');
        tmp.textContent = el.value;
        tmp.style.visibility = 'hidden';
        tmp.style.whiteSpace = 'pre';
        el.parentNode.appendChild(tmp);
        const width = tmp.getBoundingClientRect().width;
        el.parentNode.removeChild(tmp);
        return width;
    }

    function resizeElement(id, extra, minpx) {
        const el = document.getElementById(id);
        el.style.width = `calc(${Math.max(minpx, getInputWidth(el) * 0.85)}px + ${extra}ch`;
    }

    function resizeElements() {
        resizeElement('numberIn', 1, 50);
        resizeElement('errorDigits', 2, 0);
        resizeElement('maxDen', 4, 0);
    }

    // TODO(shreevatsa): Less hacky way of adjusting the error. 
    // -- (1) Look at the last error itself, not digits seen; (2) Don't recompute fractions.
    function updateTable(fun, num, den) {
        const out = document.getElementById("approximations");
        while (out.children.length > 1) {
            out.removeChild(out.lastElementChild);
        }
        let howEnd = `(Done)`;
        let added = 0;
        for (let [n, d, isConvergent] of fun(num, den)) {
            added += 1;
            if (d > document.getElementById("maxDen").value) {
                howEnd = `(More with larger denominators)`;
                break;
            } else if (added > 501) {
                howEnd = `(Too many to display on a web page!)`;
                break;
            }
            const sawNonZero = { saw: null };
            const errorDigits = document.getElementById("errorDigits");
            out.appendChild(renderFraction(n, d, num, den, errorDigits.value, sawNonZero, isConvergent));
            if (sawNonZero.saw == null) {
                console.log(`Didn't see a nonzero digit, so increasing from ${errorDigits.value}`);
                errorDigits.value = `${+errorDigits.value + 1}`;
                updateTable(fun, num, den);
                return;
            }
            // await sleepSeconds(0.5);
        }
        const tr = document.createElement('tr');
        const all = document.createElement('th');
        all.colSpan = 3;
        all.textContent = howEnd;
        tr.appendChild(all);
        out.appendChild(tr);
    }

    function insertBreaks(s) {
        s = String(s);
        let out = '';
        for (let n = 0; n < s.length; n += 1) {
            if (n > 0) {
                out += '​'; // zero-width space
            }
            out += s[n];
        }
        return out;
    }

    function updateOutput() {
        resizeElements();
        const frac = parseFracFromString(document.getElementById("numberIn").value);
        let num, den;
        if (frac == null) {
            document.getElementById('numberShow').textContent = 'invalid';
            return;
        } else {
            [num, den] = frac;
            document.getElementById("numberShow").textContent = `${insertBreaks(num)}${slash}${insertBreaks(den)}`;
        }
        let fun = ((document.getElementById("whichBest").value == "bestest") ?
            convergents : bestRationalApproximations);
        updateTable(fun, num, den);
    }
    updateOutput();
    for (let el of ["numberIn", "errorDigits", "maxDen", "whichBest"]) {
        document.getElementById(el).addEventListener("keyup", updateOutput);
        document.getElementById(el).addEventListener("change", updateOutput);
    }
</script>
