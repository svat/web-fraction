// Extract info from a floating-point number.

// The sign, exponent, and significand of a floating-point number.
function floatPartsLittleEndian(x) {
  x = Number(x);
  let float = new Float64Array(1); float[0] = x;
  const bytes = new Uint8Array(float.buffer);
  // A JS Number is an IEEE-764 double-precision floating-point number:
  //      <1 sign bit> <11 exponent bits> <52 significand bits>
  // The bits, aligned to byte boundaries (assuming little-endian):
  //     <sign> <7 of exp> <4 of exp> <4 of sig> <48 bits of sig>
  //     \----bytes[7]---/ \------bytes[6]-----/ \--bytes[5..0]-/
  const sign = (bytes[7] >> 7) ? -1n : 1n;
  const exponentBits = ((bytes[7] & 0x7f) << 4) + (bytes[6] >> 4);
  const exponent = exponentBits + (exponentBits == 0) - 1023 - 52;
  if (exponentBits == 2047) {
    // TODO(shreevatsa): Do something more sensible here.
    return { significand: 'infinity' };
  }
  let sigBits = BigInt(bytes[6] & 15);
  for (let i = 5; i >= 0; --i) {
    sigBits = (sigBits * 256n) + BigInt(bytes[i]);
  }
  const significand = sigBits + (exponentBits == 0 ? 0n : BigInt(Math.pow(2, 52)));
  console.log(`Returning ${sign} times 2^${exponent} times ${significand}`);
  return { sign, exponent, significand };
}

export function floatParts(x) {
  // TODO(shreevatsa): Detect endianness and do the right thing?
  return floatPartsLittleEndian(x);
}

